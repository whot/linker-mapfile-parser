#!/usr/bin/env python3

from pyparsing import (Group, OneOrMore, Suppress, Literal, Word, alphanums,
                       LineEnd, Optional, cStyleComment)
import sys


class LinkerABI(object):
    '''
    .. attribute:: version

        The version string of this ABI version (e.g. V_0.1.0)

    .. attribute:: parent

        The parent version object or None

    .. attribute:: globals

        List with all function names defined in the globals section of in this
        version. This list may be a single element '*' in case of a wildcard.

    .. attribute:: locals

        List with all function names defined in the locals section of in this
        version. This list may be a single element '*' in case of a wildcard.
    '''
    def __init__(self, version=None, parent=None, globals=None, locals=None):
        self.version = version
        self.parent = parent
        self.globals = globals or []
        self.locals = locals or []

    def __str__(self):
        g, l = '', ''
        if self.globals:
            g = '\n'.join(['    {};'.format(x) for x in self.globals])
        if self.locals:
            l = '\n'.join(['    {};'.format(x) for x in self.locals])

        return '{} {{\n{}{}\n{}{}{}}}{};'.format(
            self.version,
            'global:\n' if g else '',
            g,
            'local:\n' if l else '',
            l,
            '\n' if l else '',
            ' {}'.format(self.parent.version) if self.parent else '')

    def __repr__(self):
        return str(self)


def parse_linker_map(filename):
    EMPTYLINE = Suppress(LineEnd())
    SEMICOLON = Literal(';').suppress()
    COLON = Literal(':').suppress()
    LBRACE = Literal('{').suppress()
    RBRACE = Literal('}').suppress()
    GLOBAL_SCOPE = Literal('global') + COLON
    LOCAL_SCOPE = Literal('local') + COLON
    WILDCARD = Literal('*') + SEMICOLON
    FUNCTION = Word(alphanums + '_') + Suppress(SEMICOLON)
    VERSION = Word(alphanums + '._')

    # globals: may be missing and that's fine
    GLOBAL_SET = Suppress(Optional(GLOBAL_SCOPE)) + Group(OneOrMore(FUNCTION))
    # We only parse the case we actually need, which is
    #  local:
    #    *;
    LOCAL_SET = Suppress(LOCAL_SCOPE) + Group(WILDCARD)

    # Each ABI can specify one global: and one optional local:
    SET = LBRACE + Group(GLOBAL_SET + Optional(LOCAL_SET)) + RBRACE
    # First ABI doesn't have a dependency, the others do
    # newversion {
    #    .... }
    # oldversion;
    ABI = Group(VERSION + SET + Optional(VERSION) + Suppress(SEMICOLON))

    Grammar = OneOrMore(ABI)
    Grammar.ignore(cStyleComment)
    Grammar.ignore(EMPTYLINE)

    # Generated tuple is a list of (version, [globals, locals], parent)
    parsed = Grammar.parseFile(filename)

    abis = []
    for v in parsed:
        funcs = v[1]
        g = funcs[0]
        l = funcs[1] if len(funcs) > 1 else []
        abi = LinkerABI(version=v[0], globals=g, locals=l)

        for pabi in abis:
            if pabi.version == v[2]:
                abi.parent = pabi
                break
        abis.append(abi)

    return abis


if __name__ == '__main__':
    maps = parse_linker_map(sys.argv[1])

    for m in maps:
        print('{}'.format(m))
        print('')
