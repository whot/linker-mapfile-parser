Parses a GNU ld-compatible version script into a python struct,
see https://sourceware.org/binutils/docs/ld/VERSION.html
